package com.example.demo.exception.dao;
import com.example.demo.DemoApplication;
import com.example.demo.exception.exptions.AucunDocumentTrouveException;
import com.example.demo.exception.module.Document;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;


import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

//@RunWith(SpringRunner.class)
//@ContextConfiguration(classes = DemoApplication.class, loader = AnnotationConfigContextLoader.class)
@SpringBootTest
@RunWith(SpringRunner.class)
public class DslRepositoryTest {

    @Autowired
    DslRepository dslRepository;

    @Test
    public void findDocByNameTest() {
        //given
        String titre ="HTML";
        Document document =new Document();
      document.setTitre(titre);
        document.setAuthor("hajar");
       document.setDate_publication(LocalDate.of(1992, Month.DECEMBER, 15));
      dslRepository.saveDocument(document);
        //when
       boolean actuel = dslRepository.findDocByName("hhhh").size() >0 ;
        //then
       assertTrue(actuel);

    }

//    @Test(expected= AucunDocumentTrouveException.class)
@Test
    public void searchDslTest() {
        //given
        String motCle ="spring";
        Document document =new Document();
        document.setTitre("spring boot framework");
        document.setAuthor("hajar");
        document.setDate_publication(LocalDate.of(1992, Month.DECEMBER, 15));
        dslRepository.saveDocument(document);
        //when
        boolean  expected = dslRepository.searchDsl(motCle).size()>0;
        //then
        assertTrue(expected);
    }
}