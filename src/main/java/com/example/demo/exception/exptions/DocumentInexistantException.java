package com.example.demo.exception.exptions;

public class DocumentInexistantException extends Exception {
    public DocumentInexistantException(String message) {
        super(message);
    }
}
