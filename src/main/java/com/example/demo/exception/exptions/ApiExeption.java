package com.example.demo.exception.exptions;

import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

public class ApiExeption {
    private final  String message;
  //  private final Throwable throwable;//the client can have full visibility of what happend
    private HttpStatus httpStatus;
    private  ZonedDateTime timestame;
     private String errorCode;


    public ApiExeption(String message, HttpStatus httpStatus, ZonedDateTime timestame,String errorCode) {
        this.message = message;
     //   this.throwable = throwable;
        this.httpStatus = httpStatus;
        this.timestame = timestame;
        this.errorCode = errorCode;
    }

    public ApiExeption(String localizedMessage) {
        this.message = localizedMessage;
    }


    public String getMessage() {
        return message;
    }

   // public Throwable getThrowable() {return throwable;}

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public ZonedDateTime getTimestame() {
        return timestame;
    }

    public String getErrorCode() {
        return errorCode;
    }
}

