package com.example.demo.exception.exptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(value = {AucunDocumentTrouveException.class,DocumentInexistantException.class,ListeDocumentsVideException.class})
    public ResponseEntity<Object> handleApiRequestException(Exception e){
        //1 Create payload containing exeption detailes
        HttpStatus notFound =HttpStatus.NOT_FOUND ;
        String error = "9999";
      ApiExeption Z=  new ApiExeption(
             e.getMessage(),
                notFound,
                ZonedDateTime.now(ZoneId.of("Z")),
              "error");
        //2 return respense entitiy

        return new ResponseEntity<>(Z,notFound);
    }
    @ExceptionHandler(value = DocTokenExeption.class)
    public ResponseEntity<Object> docExeption(DocTokenExeption e){
        HttpStatus conflict =HttpStatus.CONFLICT;
        String error = "955";
        ApiExeption Z=  new ApiExeption(
                e.getMessage(),
                conflict,
                ZonedDateTime.now(ZoneId.of("Z")),
        "error");
        return new ResponseEntity<>(Z,conflict);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> handleValidation( ConstraintViolationException exception) {
        List<String> messages = new ArrayList<>();

        for (ConstraintViolation<?> constraintViolation : exception.getConstraintViolations()) {
            messages.add(constraintViolation.getMessage());
        }
        String messageGlobal = String.join(", ", messages);
        ApiExeption Z = new ApiExeption(messageGlobal);
        return new ResponseEntity<>(Z,HttpStatus.INTERNAL_SERVER_ERROR);


    }
}
