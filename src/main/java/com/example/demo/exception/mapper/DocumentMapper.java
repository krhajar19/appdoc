package com.example.demo.exception.mapper;

import com.example.demo.exception.module.Document;
import com.example.demo.exception.module.DocumentDtoEntity;
import fr.xebia.extras.selma.IoC;
import fr.xebia.extras.selma.Mapper;
@Mapper(withIgnoreFields = {"id","date_publication"} , withIoC = IoC.SPRING)
public interface DocumentMapper {
    DocumentDtoEntity asDocumentDto(Document myDoc);
    Document asDocument(DocumentDtoEntity myDocDto);

}
