package com.example.demo.exception.dao;

import com.example.demo.exception.module.Document;
import com.example.demo.exception.module.QDocument;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class DslRepositoryImpl implements DslRepository {
    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    DocumentRepository repository;
    public static QDocument qDocument =QDocument.document;

    @Override
    public List<Document> findDocByName(String titre) {

         JPAQuery<Document> query =new JPAQuery<>(entityManager);
        return query
                .from(qDocument)
                .where(qDocument.titre.equalsIgnoreCase(titre))
                .fetch();


    }

    @Override
    public List<Document> searchDsl(String motCle) {
        //query dsl

        JPAQuery<Document> jpaQuery =new JPAQuery<>(entityManager);
        return jpaQuery
                .from(qDocument)
                .where(qDocument.titre.contains(motCle))
                .fetch();



    }

    @Override
    public Document saveDocument(Document document) {
        return this.repository.save(document);
    }
}
