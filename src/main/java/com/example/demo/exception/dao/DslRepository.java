package com.example.demo.exception.dao;

import com.example.demo.exception.module.Document;
import com.example.demo.exception.module.QDocument;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DslRepository{
   List<Document> findDocByName(String titre) ;
   List<Document> searchDsl(String motCle);
   Document saveDocument(Document document);

}

