package com.example.demo.exception.dao;

import com.example.demo.exception.module.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

//    @Query("SELECT d FROM Document d WHERE d.titre = ?1")
//    Optional<Document> findDocByName(String titre) ;
//    @Query("SELECT d FROM Document d WHERE d.titre like %?1%")
//    List<Document>  search(String motCle);
}
