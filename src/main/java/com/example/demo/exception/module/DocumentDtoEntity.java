package com.example.demo.exception.module;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
public class DocumentDtoEntity {
    @Size(min=6, max=50 , message = "the title is not valid ")
    private String titre;
    @Size(min=6, max=50, message ="the author name is ot valid ")
    private String author;

}
