package com.example.demo.exception.controller;

import com.example.demo.exception.datasource.DocRequest;
import com.example.demo.exception.exptions.AucunDocumentTrouveException;
import com.example.demo.exception.exptions.DocumentInexistantException;
import com.example.demo.exception.exptions.ListeDocumentsVideException;
import com.example.demo.exception.exptions.DocTokenExeption;
import com.example.demo.exception.module.Document;
import com.example.demo.exception.module.DocumentDtoEntity;
import com.example.demo.exception.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class DocumentController {

    @Autowired
    DocumentService documentService;
    @PostMapping("adddoc")
    public void addDoc ( @Valid @RequestBody DocRequest request) throws DocTokenExeption {
         documentService.add(request);
    }
    @GetMapping("/api/document")
    public List<Document> getDocuments() throws ListeDocumentsVideException {

            return documentService.getDocuments();

    }

    @GetMapping("/api/document/{id}")
    public Document  getDocById(@PathVariable("id") Long documentId) throws DocumentInexistantException {
        return documentService.getDocById(documentId);
    }
    @GetMapping(value = "/api/document",params = "motCle")
    public  List<Document>  getDocByCle(@RequestParam("motCle") String motCle) throws AucunDocumentTrouveException {
        return documentService.seacheByCle(motCle);
    }
    @GetMapping(path = "/api/documentdto/{id}")
     public DocumentDtoEntity docDto (@PathVariable("id") Long doc) throws DocumentInexistantException {
        return  documentService.newMapperDto(doc);
    }
    @PostMapping(path = "/api/newdoc")
    public Document docDto(@RequestBody DocumentDtoEntity document) {
        return  documentService.newMapper(document);
    }

}

