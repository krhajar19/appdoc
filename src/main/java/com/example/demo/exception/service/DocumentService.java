package com.example.demo.exception.service;


import com.example.demo.exception.dao.DslRepositoryImpl;
import com.example.demo.exception.datasource.DocRequest;
import com.example.demo.exception.exptions.DocTokenExeption;
import com.example.demo.exception.mapper.DocumentMapper;
import com.example.demo.exception.module.Document;
import com.example.demo.exception.dao.DocumentRepository;
import com.example.demo.exception.exptions.AucunDocumentTrouveException;
import com.example.demo.exception.exptions.DocumentInexistantException;
import com.example.demo.exception.exptions.ListeDocumentsVideException;
import com.example.demo.exception.module.DocumentDtoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class DocumentService {

    @Autowired
    DocumentRepository documentRepository;
    @Autowired
    DslRepositoryImpl dslRepository;
    @Autowired
    DocumentMapper documentMapper;



    public List<Document> getDocuments() throws ListeDocumentsVideException {
        if(documentRepository.findAll().isEmpty()){
              throw new ListeDocumentsVideException("la liste est vide ");
        }

        return documentRepository.findAll();
    }

    public void add(DocRequest request) throws DocTokenExeption {
        List<Document> docOptional = dslRepository.findDocByName(request.getTitre());
        if (!docOptional.isEmpty()) {
           throw new DocTokenExeption("THE Doc is aready token ");
        }
            Document doc = new Document(
                   /* request.getTitre(),
                    request.getAuthor(),
                    request.getDate()*/
            );
        doc.setTitre(request.getTitre());
        doc.setAuthor(request.getAuthor());
        doc.setDate_publication(request.getDate_publication());
        documentRepository.save(doc);
    }



    public Document getDocById(Long documentId) throws DocumentInexistantException  {
        return documentRepository.findById(documentId)
                .orElseThrow(()->new DocumentInexistantException("document with id " + documentId + " does not exists" ));
    }

    public List<Document> seacheByCle(String motCle) throws AucunDocumentTrouveException {
       List<Document>  doc = dslRepository.searchDsl(motCle);
       if (doc.isEmpty()){
          throw new AucunDocumentTrouveException("Aucun doc exist");
      }


        return doc;
    }
   public DocumentDtoEntity newMapperDto(long myDoc) throws DocumentInexistantException {
       Optional<Document> document = this.documentRepository.findById(myDoc);
       if (!document.isPresent()) {
           throw new DocumentInexistantException("test");
       }
       System.out.println(document.get().getAuthor());
        DocumentDtoEntity docDto = documentMapper.asDocumentDto(document.get());
       System.out.println(docDto.getAuthor());
    return docDto;
     }
    public Document newMapper(DocumentDtoEntity documentDtoEntity)  {
        Document  newDocument = documentMapper.asDocument(documentDtoEntity) ;
        newDocument.setDate_publication( LocalDate.now());
        Document retunDoc = this .documentRepository.save(newDocument);
        return retunDoc;

    }
}
