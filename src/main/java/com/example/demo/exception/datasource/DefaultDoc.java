package com.example.demo.exception.datasource;

import com.example.demo.exception.module.Document;
import com.example.demo.exception.dao.DocumentRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Month;

@Component @AllArgsConstructor
public class DefaultDoc implements CommandLineRunner {

    @Autowired
    private final DocumentRepository documentRepository;

    @Override
    public void run(String... args) throws Exception {
       Document doc = new Document();
       doc.setId(1l);
       doc.setTitre("java");
       doc.setAuthor("hajar");
       doc.setDate_publication(LocalDate.of(1992, Month.DECEMBER, 15));
        Document doc1 = new Document();
        doc1.setId(2l);
        doc1.setTitre("C3");
        doc1.setAuthor("sara");
        doc1.setDate_publication(LocalDate.of(1992, Month.DECEMBER, 15));
        Document doc2 = new Document();
        doc2.setId(3l);
        doc2.setTitre("java");
        doc2.setAuthor("hajar");
        doc2.setDate_publication(LocalDate.of(1992, Month.DECEMBER, 15));
        try{
           documentRepository.save(doc);
            documentRepository.save(doc1);
            documentRepository.save(doc2);
        }catch (Exception ignored){}

    }
}
