package com.example.demo.exception.datasource;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class DocRequest {
    private final String titre;
    private final String author;
    private final LocalDate date_publication;}
